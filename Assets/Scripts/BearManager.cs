﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    [System.Serializable]
    public class TableSettings
    {
        [Range(5, 15)]
        public int Colums = 7;
        [Range(5, 15)]
        public int Rows = 7;
        [Range(3, 10)]
        public int BearColorCount = 5;
        public Vector2 BearStartPosition;
        public Vector2 BearPositionOffset;
    }
    [System.Serializable]
    public class BearSettings
    {
        public AnimationClip BearMoveAnimation = null;
        public AnimationClip BearWrongAnimation = null;
        public AnimationClip BearDeleteAnimation = null;
        public AnimationClip BearHintAnimation = null;
        public List<Sprite> BearImages = null;
        public GameObject BearPrefub = null;
    }

    public partial class BearManager : MonoBehaviour
    {
        public TableSettings tableSettings;
        public BearSettings bearSettings;
        public static Dictionary<int, Bear> BearMap = new Dictionary<int, Bear>();
        public static Dictionary<int, Vector3> BearPositionMap = new Dictionary<int, Vector3>();

        private GameObject   SelectedBear;
        private bool         BearAnimActive = false;

        private System.Random m_rand = new System.Random();

        public void Start()
        {
            tableSettings.Rows++;
            if (Match3Manager.Instance == null)
            {
                Match3Manager.Instance = new Match3Manager();
            }
            Match3Manager.Instance.Start(tableSettings.Colums, tableSettings.Rows);

            for (int i = 0; i < tableSettings.Colums * tableSettings.Rows; i++)
            {
                BearMap.Add(i, new Bear());
                BearPositionMap.Add(i, new Vector3());
            }

            GenerateTable();
        }

        private void GenerateTable()
        {
            GameObject bear = bearSettings.BearPrefub;

            GameObject field = GameObject.Find("Field");

            for (int i = 0; i < tableSettings.Rows; i++)
            {
                for (int j = 0; j < tableSettings.Colums; j++)
                {
                    float x = tableSettings.BearStartPosition.x + tableSettings.BearPositionOffset.x * j;
                    float y = tableSettings.BearStartPosition.y - tableSettings.BearPositionOffset.y * i;
                    Vector3 position = new Vector3(x, y, 0.0f);

                    GameObject bearObj = Instantiate(bear);
                    bearObj.name = "Bear_" + (i+1) + (j+1);

                    bearObj.transform.parent = field.transform;
                    bearObj.transform.position = new Vector3(x, tableSettings.BearStartPosition.y, 0);

                    int next = m_rand.Next(1, tableSettings.BearColorCount);
                    bearObj.GetComponent<SpriteRenderer>().sprite = bearSettings.BearImages[next];
                    int numb = i * tableSettings.Colums + j;
                    BearMap[numb].Init(bearObj, next);

                    x = bearObj.transform.position.x;
                    y = position.y + field.transform.localPosition.y;
                    float z = bearObj.transform.position.z;
                    BearPositionMap[numb] = new Vector3(x, y, z);
                }
            }
        }

        private bool UpdateBearPositions()
        {

            bool result = false;
            for (int i = BearMap.Count() - 1; i >= 0; i--)
            {
                if ((BearMap[i].GetState() == Bear.State.Idle || BearMap[i].GetState() == Bear.State.Delete) && BearMap[i].bearObj.transform.position != BearPositionMap[i])
                {
                    BearMap[i].StartMoveBear(BearPositionMap[i]);
                    result = true;
                }
            }
            return result;
        }

        public void Update()
        {
            BearAnimActive = false;

            foreach (var bear in BearMap)
            {
                bear.Value.Update();
                if (bear.Value.GetState() != Bear.State.Idle && bear.Value.GetState() != Bear.State.Delete)
                    BearAnimActive = true;
            }
            if (!BearAnimActive)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

                    RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
                    if (hit.collider != null)
                    {
                        SelectedBear = hit.collider.gameObject;
                    }
                    else
                    {
                        SelectedBear = null;
                    }
                }

                if (Input.GetMouseButton(0))
                {
                    if (Input.GetAxis("Mouse X") < 0 && SelectedBear != null)
                    {
                        Debug.Log("Swipe left");
                        SwipeBear(Move.left);
                    }
                    if (Input.GetAxis("Mouse X") > 0 && SelectedBear != null)
                    {
                        Debug.Log("Swipe right");
                        SwipeBear(Move.right);
                    }
                    if (Input.GetAxis("Mouse Y") < 0 && SelectedBear != null)
                    {
                        Debug.Log("Swipe down");
                        SwipeBear(Move.down);
                    }
                    if (Input.GetAxis("Mouse Y") > 0 && SelectedBear != null)
                    {
                        Debug.Log("Swipe up");
                        SwipeBear(Move.up);
                    }
                }
                

                List<int> bearsToDel = Match3Manager.Instance.FindMatch3();
                if (!UpdateBearPositions())
                {
                    if (bearsToDel.Count() > 0)
                    {
                        DeleteMach3(bearsToDel);
                        BearAnimActive = true;
                    }
                    if (!BearAnimActive)
                    {
                        if (!MoveBearsAfterDel())
                        {
                            SetNewBears();
                        }
                    }
                }
            }  
        }

        public GameObject GetBearToSwipe(BearManager.Move move)
        {
            var result = SelectedBear.name.Substring(SelectedBear.name.Length - 2);
            int row = (int)Char.GetNumericValue(result[0]);
            int col = (int)Char.GetNumericValue(result[1]);
            Debug.Log("row = " + row);
            Debug.Log("col = " + col);
            if (move == Move.left)
                col--;
            else if (move == Move.right)
                col++;
            else if (move == Move.down)
                row++;
            else
                row--;

            if (col <= 0 || row <= 0 || col > tableSettings.Colums || row > tableSettings.Rows)
                return null;

            string resObjName = "Bear_" + row.ToString() + col.ToString();

            return GameObject.Find(resObjName);
        }

        public void SwipeBear(Move move, GameObject swipeBear = null)
        {
            if (swipeBear == null)
            {
                swipeBear = GetBearToSwipe(move);
                if (!Match3Manager.Instance.HasPossibleMatch(swipeBear, SelectedBear))
                {
                    Debug.Log("<color=red>Error: </color>Match not found");
                    GetBearByObject(SelectedBear).SetState(Bear.State.Wrong);
                    GetBearByObject(swipeBear).SetState(Bear.State.Wrong);
                    SelectedBear = null;
                    return;
                }
            }

            while (!swipeBear)
            {
                swipeBear = GetBearToSwipe(move);
            }

            int firstbearMatrixNumb = Int32.Parse(SelectedBear.name.Substring(SelectedBear.name.Length - 2));
            int secondbearMatrixNumb = Int32.Parse(swipeBear.name.Substring(swipeBear.name.Length - 2));
            int firstBearNumb = TramsformMatrixValueToArrayValue(tableSettings.Colums, firstbearMatrixNumb);
            int secondBearNumb = TramsformMatrixValueToArrayValue(tableSettings.Colums, secondbearMatrixNumb); 

            Bear temp = BearMap[firstBearNumb];
            BearMap[firstBearNumb] = BearMap[secondBearNumb];
            BearMap[secondBearNumb] = temp;

            string tempName = SelectedBear.name;
            SelectedBear.name = swipeBear.name;
            swipeBear.name = tempName;

            SelectedBear = null;
        }

        public bool MoveBearsAfterDel()
        {
            bool result = false;
            for (int i = BearMap.Count() - 1; i >= 0; i--)
            {
                if (BearMap[i].spriteNumb == -1)
                {
                    int bearNumbForMove = FindPrevBearWithSprite(i);
                    if (bearNumbForMove != -1 && !BearMap[bearNumbForMove].AnimActive && !BearMap[i].AnimActive)
                    {
                        SelectedBear = BearMap[i].bearObj;
                        SwipeBear(Move.down, BearMap[bearNumbForMove].bearObj);
                        result = true;
                    }
                }
            }
            return result;
        }

        private void SetNewBears()
        {
            for (int i = BearMap.Count() - 1; i >= 0; i--)
            {
                if (BearMap[i].spriteNumb == -1)
                {
                    float x = BearMap[i].bearObj.transform.position.x;
                    float y = tableSettings.BearStartPosition.y;
                    float z = BearMap[i].bearObj.transform.position.z;
                    BearMap[i].bearObj.transform.position = new Vector3(x, y, z);

                    int next = m_rand.Next(1, tableSettings.BearColorCount);
                    if (bearSettings.BearImages.Count >= next)
                    {
                        int bearN = Int32.Parse(BearMap[i].bearObj.name.Substring(BearMap[i].bearObj.name.Length - 2));
                        int numb = TramsformMatrixValueToArrayValue(tableSettings.Colums, bearN);
                        BearMap[numb].spriteNumb = next;
                        BearMap[i].bearObj.GetComponent<SpriteRenderer>().sprite = bearSettings.BearImages[next];
                    }
                }
            }
        }

        public int FindPrevBearWithSprite(int numb)
        {
            int bearN = Int32.Parse(BearMap[numb].bearObj.name.Substring(BearMap[numb].bearObj.name.Length - 2));
            while (numb > tableSettings.Colums - 1)
            {
                numb -= tableSettings.Colums;
                if (BearMap[numb].bearObj != null && Int32.Parse(BearMap[numb].bearObj.name.Substring(BearMap[numb].bearObj.name.Length - 2)) < bearN && BearMap[numb].bearObj.GetComponent<SpriteRenderer>().sprite != null)
                    return numb;
            }
            return -1;
        }

        public bool DeleteMach3(List<int> bearsToDel)
        {
            bool hasMatch = false;
            SelectedBear = null;

            foreach (var go in BearMap)
            {
                if (go.Value.bearObj)
                {
                    int bearN = Int32.Parse(go.Value.bearObj.name.Substring(go.Value.bearObj.name.Length - 2));
                    if (bearsToDel.Contains(bearN))
                    {
                        if (go.Value.GetState() == Bear.State.Idle)
                        {
                            go.Value.SetState(Bear.State.PrepareToDelete);
                        }
                        else
                        {
                            go.Value.bearObj.GetComponent<SpriteRenderer>().sprite = null;
                            go.Value.spriteNumb = -1;

                            bearsToDel.Remove(bearN);
                            hasMatch = true;
                        }
                    }
                }
            }
            return hasMatch;
        }

        private int TramsformMatrixValueToArrayValue(int n, int value)
        {
            return ((value / 10) - 1) * n + ((value % 10) - 1);
        }

        public Bear GetBearByObject(GameObject gameObject)
        {
            Bear bear = null;
            foreach(var obj in BearMap)
            {
                if (obj.Value.bearObj == gameObject)
                {
                    bear = obj.Value;
                }
            }
            return bear;
        }
    }
}
