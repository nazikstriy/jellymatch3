﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public void Start()
    {
        GameObject bestScore = GameObject.Find("Best_Value");
        bestScore.GetComponent<Text>().text = GamePlayMenu.BestScore.ToString();
    }

    public void PlayButtonPressed()
	{
		SceneManager.LoadScene("GamePlay");
	}

	public void SettingsButtonPressed()
	{
		//TODO	
	}
	
	public void StarsButtonPressed()
	{
		//TODO	
	}
	
	public void StoreButtonPressed()
	{
		//TODO	
	}
}
