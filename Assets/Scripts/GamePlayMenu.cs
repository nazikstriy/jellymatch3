﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayMenu : MonoBehaviour
{
    public static int BestScore { get; set; }
    public static int Score { get; set; }
    private int currentScore = 0;

    public void Start()
    {
        currentScore = Score;

    }

    public void Update()
    {
        if (currentScore != Score)
        {
            currentScore = Score;
            GameObject score = GameObject.Find("Score");
            score.GetComponent<Text>().text = currentScore.ToString();
        }
        if (Score > BestScore)
        {
            BestScore = Score;
        }
    }

    public void PauseButtonPressed()
	{
		SceneManager.LoadScene("MainMenu");
	}

	public void SettingsButtonPressed()
	{
		//TODO
	}
}
