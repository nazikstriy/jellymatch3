﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public class Match3Manager
    {
        public static Match3Manager Instance;

        private int Colums;
        private int Rows;

        #region Hint
        public List<int> FindHintMatch3()
        {
            List<List<List<int>>> matchedBears = FindHintMatch();
            List<int> bearsToHint = new List<int>();

            return bearsToHint;
        }

        private List<List<List<int>>> FindHintMatch()
        {
            List<List<List<int>>> result = new List<List<List<int>>>();

            for (int row = 0, col = 0; ((row + 1) * (col + 1)) < BearManager.BearMap.Count();)
            {
                result.Add(FindMachForObject(row, col));

                if (col == Colums - 1)
                {
                    row++;
                    col = 0;
                }
                else
                {
                    col++;
                }
            }
            return result;
        }
        #endregion

        #region Match3
        public List<int> FindMatch3()
        {
            List<List<List<int>>> matchedBears = FindMatch();
            List<int> bearsToDel = new List<int>();

            foreach (var mathcedBear in matchedBears)
            {
                if (mathcedBear.Count() > 0)
                {
                    if (mathcedBear[0].Count() > 2)
                    {
                        GamePlayMenu.Score += (mathcedBear[0].Count() - 1) * 5;
                        foreach (var numb in mathcedBear[0])
                        {
                            if (!bearsToDel.Contains(numb))
                            {
                                bearsToDel.Add(numb);
                            }
                        }
                    }
                    if (mathcedBear.Count() > 1 && mathcedBear[1].Count() > 2)
                    {
                        foreach (var numb in mathcedBear[1])
                        {
                            if (!bearsToDel.Contains(numb))
                            {
                                bearsToDel.Add(numb);
                            }
                        }
                    }
                }
            }
            return bearsToDel;
        }

        private List<List<List<int>>> FindMatch()
        {
            List<List<List<int>>> result = new List<List<List<int>>>();

            for (int row = 1, col = 0; ((row + 1) * (col + 1)) < BearManager.BearMap.Count();)
            {
                result.Add(FindMachForObject(row, col));

                if (col == Colums - 1)
                {
                    row++;
                    col = 0;
                }
                else
                {
                    col++;
                }
            }
            return result;
        }

        private List<List<int>> FindMachForObject(int row, int col)
        {
            List<List<int>> result = new List<List<int>>();

            var res = FindVerticalMatch(row, col);
            if (res != null)
                result.Add(res);

            res = FindHorizontalMatch(row, col);
            if (res != null)
                result.Add(res);

            return result;
        }

        private List<int> FindVerticalMatch(int row, int col)
        {
            List<int> result = new List<int>();
            int i = row;

            while (i < Rows && BearManager.BearMap[(row * Colums) + col].spriteNumb != -1 && BearManager.BearMap[(row * Colums) + col].spriteNumb == BearManager.BearMap[(i * Colums) + col].spriteNumb)
            {
                result.Add(((i + 1) * 10) + col + 1);
                i++;
            }
            i = row - 1;
            while (i > 0 && BearManager.BearMap[(row * Colums) + col].spriteNumb != -1 && BearManager.BearMap[(row * Colums) + col].spriteNumb == BearManager.BearMap[(i * Colums) + col].spriteNumb)
            {
                result.Add(((i + 1) * 10) + col + 1);
                i--;
            }

            if (result.Count() >= 3)
            {
                return result;
            }

            return null;
        }

        private List<int> FindHorizontalMatch(int row, int col)
        {
            List<int> result = new List<int>();
            int i = col;
            while (i < Colums && BearManager.BearMap[(row * Colums) + col].spriteNumb != -1 && BearManager.BearMap[(row * Colums) + col].spriteNumb == BearManager.BearMap[(row * Colums) + i].spriteNumb)
            {
                result.Add(((row + 1) * 10) + i + 1);
                i++;
            }
            i = col - 1;
            while (i >= 0 && BearManager.BearMap[(row * Colums) + col].spriteNumb != -1 && BearManager.BearMap[(row * Colums) + col].spriteNumb == BearManager.BearMap[(row * Colums) + i].spriteNumb)
            {
                result.Add(((row + 1) * 10) + i + 1);
                i--;
            }
            if (result.Count() >= 3)
            {
                return result;
            }
            return null;
        }
        #endregion

        #region Match Possibility
        public bool HasPossibleMatch(GameObject bear1, GameObject bear2)
        {
            int bearN = Int32.Parse(bear1.name.Substring(getBearNumberStartIndex(bear1)));
            if (FindPossibleMachForObject(bear2, bearN))
            {
                return true;
            }
            bearN = Int32.Parse(bear2.name.Substring(getBearNumberStartIndex(bear2)));
            if (FindPossibleMachForObject(bear1, bearN))
            {
                return true;
            }
            return false;
        }

        private bool FindPossibleMachForObject(GameObject bear, int bearN)
        {
            if (FindPossibleVerticalMatch(bear, getCodeNumber(bearN)))
                return true;
            if (FindPossibleHorizontalMatch(bear, getCodeNumber(bearN)))
                return true;

            return false;
        }

        private bool FindPossibleVerticalMatch(GameObject bear, int bearN)
        {
            List<int> result = new List<int>();
            int i = (bearN / 10) + 1;
            int col = bearN % 10;

            int bearNumb = getCodeNumber(Int32.Parse(bear.name.Substring(getBearNumberStartIndex(bear))));
            int bearSpriteNumb = GetArrayNumber(bearNumb);
            
            while ((i < Rows || (i == bearN / 10)) && (BearManager.BearMap[bearSpriteNumb].spriteNumb == BearManager.BearMap[GetArrayNumber(i, col)].spriteNumb))
            {
                if(bearNumb != i*10 + col)
                    result.Add(((i + 1) * 10) + col + 1);
                i++;
            }
            i = bearN / 10 - 1;
            while (i >= 0 && BearManager.BearMap[bearSpriteNumb].spriteNumb == BearManager.BearMap[GetArrayNumber(i, col)].spriteNumb)
            {
                if (bearNumb != i * 10 + col)
                    result.Add(((i + 1) * 10) + col + 1);
                i--;
            }

            if (result.Count() >= 2)
            {
                //FIX IT WHEN ALL DONE
                int min = result.Min()/10;
                int max = result.Max()/10;
                if ((max - min) > result.Count())
                    return false;
                return true;
            }

            return false;
        }

        private bool FindPossibleHorizontalMatch(GameObject bear, int bearN)
        {
            List<int> result = new List<int>();
            int i = (bearN % 10) + 1;
            int row = bearN / 10;
            int bearNumb = getCodeNumber(Int32.Parse(bear.name.Substring(getBearNumberStartIndex(bear))));
            int bearSpriteNumb = GetArrayNumber(bearNumb);

            while ((i < Colums || (i == bearN / 10)) && (BearManager.BearMap[bearSpriteNumb].spriteNumb == BearManager.BearMap[GetArrayNumber(row, i)].spriteNumb))
            {
                if (bearNumb != row * 10 + i)
                    result.Add(((row + 1) * 10) + i + 1);
                i++;
            }
            i = bearN % 10 - 1;
            while (i >= 0 && BearManager.BearMap[bearSpriteNumb].spriteNumb == BearManager.BearMap[GetArrayNumber(row, i)].spriteNumb)
            {
                if (bearNumb != row * 10 + i)
                    result.Add(((row + 1) * 10) + i + 1);
                i--;
            }
            if (result.Count() >= 2)
            {
                //FIX IT WHEN ALL DONE
                int min = result.Min();
                int max = result.Max();
                if ((max - min) > result.Count())
                    return false;
                return true;
            }
            return false;
        }
#endregion

        #region Additional functions
        int getBearNumberStartIndex(GameObject bear)
        {
            return bear.name.Length - 2;
        }

        int getCodeNumber(int nameNumb)
        {
            return nameNumb - 11;
        }

        int GetArrayNumber(int codeNumb)
        {
            return (codeNumb % 10) + ((codeNumb / 10) * Colums);
        }

        int GetArrayNumber(int row, int col)
        {
            return col + (row * Colums);
        }

        public void Start(int col, int row)
        {
            GamePlayMenu.Score = 0;
            Colums = col;
            Rows = row;
        }
#endregion
    }
}