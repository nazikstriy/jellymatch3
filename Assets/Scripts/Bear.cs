﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Bear
    {
        public GameObject   bearObj;
        public int          spriteNumb;
        public bool         AnimActive = false;

        private const float animationTime       = 250.0f;
        private const float deleteAnimationTime = 80.0f;

        private State       state               = State.Idle;
        private Vector3     position;
        private Vector3     movePosition;
        private Vector3     bearScale;
        private Animation   bearAnimation;
        private float       animTime            = 60.0f;

        public enum State
        {
            Idle,
            Move,
            Wrong,
            Hint,
            PrepareToDelete,
            Delete
        }

        public void Init(GameObject go, int sprite)
        {
            bearObj = go;
            bearScale = go.transform.localScale;
            spriteNumb = sprite;
            bearAnimation = go.GetComponent<Animation>();
            state = State.Idle;
        }

        public void Update()
        {

            if (AnimActive)
            {
                if (!bearAnimation.isPlaying)
                {
                    if (this.state == State.PrepareToDelete)
                        this.state = State.Delete;
                    else
                        this.state = State.Idle;
                    this.bearObj.transform.localScale = bearScale;
                    AnimActive = false;
                }
                if (this.state == State.PrepareToDelete && animTime > deleteAnimationTime)
                {
                    bearAnimation.Stop();
                    if (this.state == State.Wrong)
                        bearObj.transform.localPosition = movePosition;
                }
                else if (this.state != State.PrepareToDelete && animTime > animationTime)
                {
                    bearAnimation.Stop();
                    if(this.state == State.Wrong)
                        bearObj.transform.localPosition = movePosition;
                }
                else
                {
                    animTime += 1.0f;
                }
            }
        }

        public void StartMoveBear(Vector3 end)
        {
            position = bearObj.transform.localPosition;
            movePosition = end;
            MoveAnim(position, movePosition);
        }

        public void MoveAnim(Vector3 start, Vector3 end)
        {
            state = State.Move;
            AnimActive = true;
            animTime = 0.0f;
            AnimationCurve curve;

            AnimationClip clip = bearAnimation.GetClip("Move");
            Keyframe[] keys = new Keyframe[2];

            keys[0] = new Keyframe(0.0f, start.x);
            keys[1] = new Keyframe(1.0f, end.x);
            curve = new AnimationCurve(keys);
            clip.SetCurve("", typeof(Transform), "localPosition.x", curve);

            keys[0] = new Keyframe(0.0f, start.y);
            keys[1] = new Keyframe(1.0f, end.y);
            curve = new AnimationCurve(keys);
            clip.SetCurve("", typeof(Transform), "localPosition.y", curve);

            bearAnimation.AddClip(clip, "Move"+bearObj.name);
            bearAnimation.Play("Move" + bearObj.name);
        }

        private void WrongAnim()
        {
            AnimActive = true;
            SetState(State.Wrong);
            bearAnimation.Play("Wrong");
            animTime = 0.0f;
        }

        private void DeleteAnim()
        {
            AnimActive = true;
            bearAnimation.Play("Delete");
            animTime = 0.0f;
        }

        private void HintAnim()
        {
            AnimActive = true;
            bearAnimation.Play("Hint");
            animTime = 0.0f;
        }

        public State GetState()
        {
            return state;
        }

        public void SetState(State newState)
        {
            if (this.state != newState)
            {
                this.state = newState;
                switch (this.state)
                {
                    case State.Idle:
                        break;
                    case State.Hint:
                        HintAnim();
                        break;
                    case State.PrepareToDelete:
                        DeleteAnim();
                        break;
                    case State.Move:
                        break;
                    case State.Wrong:
                        WrongAnim();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
